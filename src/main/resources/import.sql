insert into sport (name, label) values ('Bike' , 'Ciclismo');
insert into sport (name, label) values ('Run'  , 'Corrida');
insert into sport (name, label) values ('Swim' , 'Natação');
insert into sport (name, label) values ('Gym'  , 'Musculação');

delete from users where email = 'admin@worbi';
insert into users (name, email, is_admin, password) values ('Administrador','admin@worbi', true, '$2a$10$eZSW0.GEguXggqT40BhM8eRMRQWNVioDC7Rwa3FaS.kCqNxBIwF/i');