package com.synit.worbiservice.util.log;

public enum LogLevel {

    INFO, DEBUG, TRACE, ERROR;

}
