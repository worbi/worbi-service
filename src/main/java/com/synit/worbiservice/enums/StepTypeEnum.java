package com.synit.worbiservice.enums;

public enum StepTypeEnum {

    WARMUP   (0, "Warm Up"),
    EXERCISE (1, "Exercise"),
    RECOVER  (2, "Recover"),
    REST     (3, "Rest"),
    COOLDOWN (4, "Cool Down");

    private Integer id;
    private String name;

    StepTypeEnum(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
