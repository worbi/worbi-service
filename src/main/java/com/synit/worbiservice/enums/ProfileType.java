package com.synit.worbiservice.enums;

public enum ProfileType {

    ATHLETE, COACH;

}
