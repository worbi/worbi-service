package com.synit.worbiservice.enums;

public enum WeekdayEnum {

    SUNDAY    (1, "Sunday", "Sun"),
    MONDAY    (2, "Monday", "Mon"),
    TUESDAY   (3, "Tuesday", "Tue"),
    WEDNESDAY (4, "Wednesday", "Wed"),
    THURSDAY  (5, "Thursday", "Thu"),
    FRIDAY    (6, "Friday", "Fri"),
    SATURDAY  (7, "Saturday", "Sat");

    private Integer id;
    private String longName;
    private String shortname;

    WeekdayEnum (Integer id, String longName,String shortName) {
        this.id = id;
        this.longName = longName;
        this.shortname = shortName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }
}
