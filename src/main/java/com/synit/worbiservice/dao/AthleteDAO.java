package com.synit.worbiservice.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.synit.worbiservice.entity.Athlete;

import java.util.List;

public interface AthleteDAO extends JpaRepository<Athlete, Long> {

    List<Athlete> findByCredentialIdUser(Long idUser);
    List<Athlete> findAllByCoachIdCoach(Long idCoach);
    List<Athlete> findAllByCredentialNameContainingIgnoreCaseOrCredentialEmailContainingIgnoreCase(String name, String email);

}
