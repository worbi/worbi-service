package com.synit.worbiservice.dao;

import com.synit.worbiservice.entity.Workout;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WorkoutDAO extends JpaRepository<Workout, Long> {

    List<Workout> findAllByAthleteIdAthleteOrderByScheduleDateDesc(Long idAthlete);

}
