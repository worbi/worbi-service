package com.synit.worbiservice.dao;

import com.synit.worbiservice.entity.Step;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StepDAO extends JpaRepository<Step, Long> {

    List<Step> findAllByOrderByStepOrderAsc();

}
