package com.synit.worbiservice.dao;

import com.synit.worbiservice.entity.Athlete;
import com.synit.worbiservice.entity.TrainingWeekday;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

public interface TrainingWeekdayDAO extends JpaRepository<TrainingWeekday, Long> {

    @Transactional
    void deleteByAthleteIdAthlete(@Param("idAthlete") Long idAthlete);
}
