package com.synit.worbiservice.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.synit.worbiservice.entity.Sport;

public interface SportDAO extends JpaRepository<Sport, Long> {

}
