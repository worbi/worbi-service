package com.synit.worbiservice.dao;

import com.synit.worbiservice.entity.Message;
import com.synit.worbiservice.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MessageDAO extends JpaRepository<Message, Long> {

    List<Message> findAllByFromOrderByReadAscSentDateDesc(User from);
    List<Message> findAllByFromAndToOrderBySentDateAsc(User from, User to);
    List<Message> findAllByToOrderByReadAscSentDateDesc(User to);
    List<Message> findAllByFromOrderBySentDateDesc(User from);
    List<Message> findAllByToOrderBySentDateAsc(User to);

}
