package com.synit.worbiservice.dao;

import com.synit.worbiservice.entity.Coach;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CoachDAO extends JpaRepository<Coach, Long> {

    List<Coach> findByCredentialIdUser(Long idUser);
    List<Coach> findAllByCredentialNameContainingIgnoreCaseOrCredentialEmailContainingIgnoreCase(String name, String email);

}
