package com.synit.worbiservice.dao;

import com.synit.worbiservice.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserDAO extends JpaRepository<User, Long> {

    User findUserByIdUser(Long idUser);
    User findByEmail(String email);
    List<User> findByNameContainingIgnoreCaseOrEmailContainingIgnoreCaseOrderByName(String Name, String email);
}