package com.synit.worbiservice.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

public class AthleteVO {

	private Long idAthlete;
	private Date birthDate;
	private String gender;
	private Double weight;
	private Double height;

	private User credential;

	private List<TrainingWeekday> trainingWeekdays;
	
	private List<Sport> sports;
	
	private List<Coach> coach;

    private List<Workout> workouts;

	public Long getIdAthlete() {
		return idAthlete;
	}

	public void setIdAthlete(Long idAthlete) {
		this.idAthlete = idAthlete;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getHeight() {
		return height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public User getCredential() {
		return credential;
	}

	public void setCredential(User credential) {
		this.credential = credential;
	}

    public List<TrainingWeekday> getTrainingWeekdays() {
        return trainingWeekdays;
    }

    public void setTrainingWeekdays(List<TrainingWeekday> trainingWeekdays) {
        this.trainingWeekdays = trainingWeekdays;
    }

    public List<Sport> getSports() {
		return sports;
	}

	public void setSports(List<Sport> sports) {
		this.sports = sports;
	}

	public List<Coach> getCoach() {
		return coach;
	}

	public void setCoach(List<Coach> coach) {
		this.coach = coach;
	}

    public List<Workout> getWorkouts() {
        return workouts;
    }

    public void setWorkouts(List<Workout> workouts) {
        this.workouts = workouts;
    }


    @Override
    public String toString() {
        return "AthleteVO{" +
                "idAthlete=" + idAthlete +
                ", birthDate=" + birthDate +
                ", gender='" + gender + '\'' +
                ", weight=" + weight +
                ", height=" + height +
                ", credential=" + credential +
                ", trainingWeekdays=" + trainingWeekdays +
                ", sports=" + sports +
                ", coach=" + coach +
                ", workouts=" + workouts +
                '}';
    }
}
