package com.synit.worbiservice.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Workout {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long idWorkout;

    @Column
    private String description;

    @Column
    private String totalTime;

    @Column
    private Date scheduleDate;

    @Column
    private Boolean done;

    @ManyToOne
    @JoinColumn(name = "idSport")
    private Sport sport;

    @ManyToOne
    @JoinColumn(name = "idAthlete")
    private Athlete athlete;

    @ManyToOne
    @JoinColumn(name = "idCoach")
    private Coach coach;

    @OneToMany(cascade=CascadeType.REMOVE, mappedBy="workout")
    private List<Step> steps;

    @Column
    private String feedback;

    public Long getIdWorkout() {
        return idWorkout;
    }

    public void setIdWorkout(Long idWorkout) {
        this.idWorkout = idWorkout;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(String totalTime) {
        this.totalTime = totalTime;
    }

    public Date getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(Date scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public Sport getSport() {
        return sport;
    }

    public void setSport(Sport sport) {
        this.sport = sport;
    }

    public Athlete getAthlete() {
        return athlete;
    }

    public void setAthlete(Athlete athlete) {
        this.athlete = athlete;
    }

    public List<Step> getSteps() {
        return steps;
    }

    public void setSteps(List<Step> steps) {
        this.steps = steps;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

    public Coach getCoach() {
        return coach;
    }

    public void setCoach(Coach coach) {
        this.coach = coach;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    @Override
    public String toString() {
        return "Workout{" +
                "idWorkout=" + idWorkout +
                ", description='" + description + '\'' +
                ", totalTime='" + totalTime + '\'' +
                ", scheduleDate=" + scheduleDate +
                ", done=" + done +
                ", sport=" + sport +
                ", athlete=" + athlete +
                ", coach=" + coach +
                ", steps=" + steps +
                ", feedback='" + feedback + '\'' +
                '}';
    }
}
