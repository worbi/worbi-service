package com.synit.worbiservice.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.OnDelete;

import javax.persistence.*;

@Entity
public class TrainingWeekday {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long idTrainingWeekday;

    @Column
    private Long weekday;

    @Column
    private Double availableTime;

    @ManyToOne
    @JoinColumn(name = "idAthlete", nullable = false, updatable = false, insertable = true)
    @JsonBackReference
    private Athlete athlete;


    public Long getIdTrainingWeekday() {
        return idTrainingWeekday;
    }

    public void setIdTrainingWeekday(Long idTrainingWeekday) {
        this.idTrainingWeekday = idTrainingWeekday;
    }

    public Long getWeekday() {
        return weekday;
    }

    public void setWeekday(Long weekday) {
        this.weekday = weekday;
    }

    public Double getAvailableTime() {
        return availableTime;
    }

    public void setAvailableTime(Double availableTime) {
        this.availableTime = availableTime;
    }

    public Athlete getAthlete() {
        return athlete;
    }

    public void setAthlete(Athlete athlete) {
        this.athlete = athlete;
    }

    @Override
    public String toString() {
        return "TrainingWeekday{" +
                "idTrainingWeekday=" + idTrainingWeekday +
                ", weekday=" + weekday +
                ", availableTime=" + availableTime +
                ", athlete=" + athlete +
                '}';
    }
}
