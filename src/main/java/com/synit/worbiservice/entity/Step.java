package com.synit.worbiservice.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.synit.worbiservice.enums.StepTypeEnum;

import javax.persistence.*;
import java.sql.Time;

@Entity
@Table(name = "step")
public class Step {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long idStep;

    @Column
    private Long repeatId;
    @Column
    private Long stepOrder;
    @Column
    private Long repeatOrder;
    @Column
    private Long repeatTimes;
    @Column
    private StepTypeEnum stepType;
    @Column
    private String timeDuration;
    @Column
    private Double distanceDuration;
    @Column
    private String description;

    @ManyToOne
    @JoinColumn(name="idWorkout", nullable=false)
    @JsonBackReference
    private Workout workout;

    public Long getIdStep() {
        return idStep;
    }

    public void setIdStep(Long idStep) {
        this.idStep = idStep;
    }

    public Long getStepOrder() {
        return stepOrder;
    }

    public void setStepOrder(Long stepOrder) {
        this.stepOrder = stepOrder;
    }

    public Long getRepeatOrder() {
        return repeatOrder;
    }

    public void setRepeatOrder(Long repeatOrder) {
        this.repeatOrder = repeatOrder;
    }

    public Long getRepeatTimes() {
        return repeatTimes;
    }

    public void setRepeatTimes(Long repeatTimes) {
        this.repeatTimes = repeatTimes;
    }

    public StepTypeEnum getStepType() {
        return stepType;
    }

    public void setStepType(StepTypeEnum stepType) {
        this.stepType = stepType;
    }

    public String getTimeDuration() {
        return timeDuration;
    }

    public void setTimeDuration(String timeDuration) {
        this.timeDuration = timeDuration;
    }

    public Long getRepeatId() {
        return repeatId;
    }

    public void setRepeatId(Long repeatId) {
        this.repeatId = repeatId;
    }

    public Double getDistanceDuration() {
        return distanceDuration;
    }

    public void setDistanceDuration(Double distanceDuration) {
        this.distanceDuration = distanceDuration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Workout getWorkout() {
        return workout;
    }

    public void setWorkout(Workout workout) {
        this.workout = workout;
    }

    @Override
    public String toString() {
        return "Step{" +
                "idStep=" + idStep +
                ", repeatId=" + repeatId +
                ", stepOrder=" + stepOrder +
                ", repeatOrder=" + repeatOrder +
                ", repeatTimes=" + repeatTimes +
                ", stepType=" + stepType +
                ", timeDuration='" + timeDuration + '\'' +
                ", distanceDuration=" + distanceDuration +
                ", description='" + description + '\'' +
                ", workout=" + workout +
                '}';
    }
}
