package com.synit.worbiservice.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.synit.worbiservice.entity.serializer.CustomAthleteSerializer;

import javax.persistence.*;
import java.util.List;

@Entity
public class Coach {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
	private Long idCoach;
	@Column
	private String cref;
	
	@ManyToOne
	@JoinColumn(name = "idUser")
	private User credential;
	
	@ManyToMany
    @JoinTable(name = "coach_sport", joinColumns = @JoinColumn(name = "idCoach"), inverseJoinColumns = @JoinColumn(name = "idSport"))
	private List<Sport> sports;
	
	@ManyToMany
    @JoinTable(name = "coach_athlete", joinColumns = @JoinColumn(name = "idCoach"), inverseJoinColumns = @JoinColumn(name = "idAthlete"))
	@JsonSerialize(using = CustomAthleteSerializer.class)
	private List<Athlete> athletes;

	public Long getIdCoach() {
		return idCoach;
	}

	public void setIdCoach(Long idCoach) {
		this.idCoach = idCoach;
	}

	public String getCref() {
		return cref;
	}

	public void setCref(String cref) {
		this.cref = cref;
	}

	public User getCredential() {
		return credential;
	}

	public void setCredential(User credential) {
		this.credential = credential;
	}

	public List<Sport> getSports() {
		return sports;
	}

	public void setSports(List<Sport> sports) {
		this.sports = sports;
	}

	public List<Athlete> getAthletes() {
		return athletes;
	}

	public void setAthletes(List<Athlete> athletes) {
		this.athletes = athletes;
	}

    @Override
    public String toString() {
        return "Coach{" +
                "idCoach=" + idCoach +
                ", cref='" + cref + '\'' +
                ", credential=" + credential +
                ", sports=" + sports +
                ", athletes=" + athletes +
                '}';
    }
}
