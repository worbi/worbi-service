package com.synit.worbiservice.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="athlete")
public class Athlete {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
	private Long idAthlete;
	@Column
	private Date birthDate;
	@Column
	private String gender;
	@Column
	private Double weight;
	@Column
	private Double height;

	@ManyToOne
	@JoinColumn(name = "idUser")
	private User credential;

	@OneToMany(mappedBy = "athlete")
	private List<TrainingWeekday> trainingWeekdays;
	
	@ManyToMany
    @JoinTable(name = "athlete_sport", joinColumns = @JoinColumn(name = "idAthlete"), inverseJoinColumns = @JoinColumn(name = "idSport"))
	private List<Sport> sports;
	
	@ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "coach_athlete", joinColumns = @JoinColumn(name = "idAthlete"), inverseJoinColumns = @JoinColumn(name = "idCoach"))
	private List<Coach> coach;

    @OneToMany(cascade=CascadeType.MERGE)
    @JoinColumn(name="idAthlete")
    @JsonBackReference
    private List<Workout> workouts;


	public Long getIdAthlete() {
		return idAthlete;
	}

	public void setIdAthlete(Long idAthlete) {
		this.idAthlete = idAthlete;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getHeight() {
		return height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public User getCredential() {
		return credential;
	}

	public void setCredential(User credential) {
		this.credential = credential;
	}

    public List<TrainingWeekday> getTrainingWeekdays() {
        return trainingWeekdays;
    }

    public void setTrainingWeekdays(List<TrainingWeekday> trainingWeekdays) {
        this.trainingWeekdays = trainingWeekdays;
    }

    public List<Sport> getSports() {
		return sports;
	}

	public void setSports(List<Sport> sports) {
		this.sports = sports;
	}

	public List<Coach> getCoach() {
		return coach;
	}

	public void setCoach(List<Coach> coach) {
		this.coach = coach;
	}

    public List<Workout> getWorkouts() {
        return workouts;
    }

    public void setWorkouts(List<Workout> workouts) {
        this.workouts = workouts;
    }

    @Override
    public String toString() {
        return "Athlete{" +
                "idAthlete=" + idAthlete +
                ", birthDate=" + birthDate +
                ", gender='" + gender + '\'' +
                ", weight=" + weight +
                ", height=" + height +
                ", credential=" + credential +
                ", trainingWeekdays=" + trainingWeekdays +
                ", sports=" + sports +
                ", coach=" + coach +
                ", workouts=" + workouts +
                '}';
    }
}
