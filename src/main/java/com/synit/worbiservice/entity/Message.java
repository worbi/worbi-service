package com.synit.worbiservice.entity;

import javax.persistence.*;
import java.sql.Date;

@Entity
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long idMessage;

    @ManyToOne
    @JoinColumn(name = "idUserTo")
    private User to;

    @ManyToOne
    @JoinColumn(name = "idUserFrom")
    private User from;

    @Column
    private String message;

    @Column
    private Date sentDate;

    @Column
    private Boolean read;

    public Long getIdMessage() {
        return idMessage;
    }

    public void setIdMessage(Long idMessage) {
        this.idMessage = idMessage;
    }

    public User getTo() {
        return to;
    }

    public void setTo(User to) {
        this.to = to;
    }

    public User getFrom() {
        return from;
    }

    public void setFrom(User from) {
        this.from = from;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    public Boolean getRead() {
        return read;
    }

    public void setRead(Boolean read) {
        this.read = read;
    }

    @Override
    public String toString() {
        return "Message{" +
                "idMessage=" + idMessage +
                ", to=" + to +
                ", from=" + from +
                ", message='" + message + '\'' +
                ", sentDate=" + sentDate +
                ", read=" + read +
                '}';
    }
}
