package com.synit.worbiservice.exception;

public class DuplicatedUserException extends Exception {

    @Override
    public String getMessage() {
        return "Duplicated User";
    }
}
