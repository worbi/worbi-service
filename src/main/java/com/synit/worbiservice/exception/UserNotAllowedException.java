package com.synit.worbiservice.exception;

public class UserNotAllowedException extends UserNotFoundException {
    @Override
    public String getMessage() {
        return "Email and/or Password are invalid";
    }
}
