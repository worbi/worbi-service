package com.synit.worbiservice.exception;

public class UserNotFoundException extends Exception {

    @Override
    public String getMessage() {
        return "User not found";
    }
}
