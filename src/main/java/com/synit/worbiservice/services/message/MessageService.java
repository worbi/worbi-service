package com.synit.worbiservice.services.message;

import com.synit.worbiservice.dao.MessageDAO;
import com.synit.worbiservice.entity.Message;
import com.synit.worbiservice.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MessageService {

    @Autowired
    private MessageDAO messageDAO;

    public List<Message> listMessageFromUser(User fromUser) {
        return messageDAO.findAllByFromOrderByReadAscSentDateDesc(fromUser);
    }

    public List<Message> listMessageFromUserToUser(User fromUser, User toUser) {
        return messageDAO.findAllByFromAndToOrderBySentDateAsc(fromUser, toUser);
    }

    public List<Message> listMessageToUser(User toUser) {
        return messageDAO.findAllByToOrderBySentDateAsc(toUser);
    }

    public Message save(Message message) {
        return messageDAO.save(message);
    }

}
