package com.synit.worbiservice.services.message;

import com.synit.worbiservice.controller.BaseController;
import com.synit.worbiservice.entity.Message;
import com.synit.worbiservice.entity.User;
import com.synit.worbiservice.services.user.UserService;
import com.synit.worbiservice.util.log.LoggingUtility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.sql.Date;
import java.util.List;

@RestController
public class MessageController extends BaseController {
    private static Logger logger = Logger.getLogger(MessageController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private MessageService messageService;

    @CrossOrigin(origins = "*")
    @GetMapping("message/from/{idFromUser}")
    public ResponseEntity<List<Message>> listMessageFrom(@PathVariable Long idFromUser) {
        final long start = System.currentTimeMillis();
        final String _methodName = "listMessageFrom(idFromUser)";

        LoggingUtility.logEntering(logger, _methodName, idFromUser.toString());

        User fromUser = userService.findById(idFromUser);
        List<Message> result = messageService.listMessageFromUser(fromUser);

        LoggingUtility.logTrace(logger, _methodName, "fromUser + ["+ fromUser +"]");
        LoggingUtility.logTrace(logger, _methodName, "messages + ["+ result +"]");

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("message/from/{idFromUser}/to/{idToUser}")
    public ResponseEntity<List<Message>> listMessageFrom(@PathVariable Long idFromUser, @PathVariable Long idToUser) {
        final long start = System.currentTimeMillis();
        final String _methodName = "listMessageFrom(idFromUser)";

        LoggingUtility.logEntering(logger, _methodName, idFromUser.toString());

        User fromUser = userService.findById(idFromUser);
        User toUser = userService.findById(idToUser);
        List<Message> result = messageService.listMessageFromUserToUser(fromUser, toUser);

        LoggingUtility.logTrace(logger, _methodName, "fromUser + ["+ fromUser +"]");
        LoggingUtility.logTrace(logger, _methodName, "messages + ["+ result +"]");

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("message/to/{idToUser}")
    public ResponseEntity<List<Message>> listMessageTo(@PathVariable Long idToUser) {
        final long start = System.currentTimeMillis();
        final String _methodName = "listMessageTo(idFromUser)";

        LoggingUtility.logEntering(logger, _methodName, idToUser.toString());

        User toUser = userService.findById(idToUser);
        List<Message> result = messageService.listMessageToUser(toUser);

        LoggingUtility.logTrace(logger, _methodName, "fromUser + ["+ toUser +"]");
        LoggingUtility.logTrace(logger, _methodName, "messages + ["+ result +"]");

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @CrossOrigin(origins = "*")
    @PostMapping("message")
    public ResponseEntity<Message> sendMessage(@RequestBody Message message) {
        final long start = System.currentTimeMillis();
        final String _methodName = "sendMessage(message)";

        LoggingUtility.logEntering(logger, _methodName, message.toString());

        if (message.getRead() == null) {
            message.setRead(Boolean.FALSE);
        }

        Message result = messageService.save(message);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

}
