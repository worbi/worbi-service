package com.synit.worbiservice.services.login;

import com.synit.worbiservice.controller.BaseController;
import com.synit.worbiservice.entity.User;
import com.synit.worbiservice.exception.UserNotFoundException;
import com.synit.worbiservice.services.login.vo.LoginUser;
import com.synit.worbiservice.util.log.LoggingUtility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController extends BaseController {

    private static Logger logger = Logger.getLogger(LoginController.class);

    @Autowired
    private LoginService loginService;

    @CrossOrigin(origins = "*")
    @PostMapping("login")
    public ResponseEntity<User> login(@RequestBody LoginUser userRequest) throws UserNotFoundException {

        final long start = System.currentTimeMillis();
        final String _methodName = "login(user)";
        LoggingUtility.logEntering(logger, _methodName, userRequest.toString());

        User authenticated = loginService.login(userRequest.getEmail(), userRequest.getPassword());

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(authenticated);
    }



}
