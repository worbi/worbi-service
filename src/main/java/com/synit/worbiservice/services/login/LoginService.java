package com.synit.worbiservice.services.login;

import com.synit.worbiservice.DatabaseConfig;
import com.synit.worbiservice.dao.UserDAO;
import com.synit.worbiservice.entity.User;
import com.synit.worbiservice.exception.UserNotAllowedException;
import com.synit.worbiservice.exception.UserNotFoundException;
import com.synit.worbiservice.util.log.LoggingUtility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LoginService {

    private static Logger logger = Logger.getLogger(LoginService.class);

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private DatabaseConfig databaseConfig;

    public User login(String email, String password) throws UserNotFoundException {
        final String _methodName = "login(email, password)";
        LoggingUtility.logEntering(logger, _methodName, email, password);

        User result = userDAO.findByEmail(email);
        if (result != null) {
            Boolean userAllowed = databaseConfig.passwordEncoder().matches(password, result.getPassword());
            if (userAllowed) {
                return result;
            } else {
                UserNotAllowedException ex = new UserNotAllowedException();
                LoggingUtility.logError(logger, _methodName, ex.getMessage());
                throw ex;
            }

        } else {
            UserNotFoundException ex = new UserNotFoundException();
            LoggingUtility.logError(logger, _methodName, ex.getMessage());
            throw ex;
        }

    }

}
