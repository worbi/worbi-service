package com.synit.worbiservice.services.workout;

import com.synit.worbiservice.controller.BaseController;
import com.synit.worbiservice.entity.Step;
import com.synit.worbiservice.entity.Workout;
import com.synit.worbiservice.util.log.LoggingUtility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@RestController
public class WorkoutController extends BaseController {
    private static Logger logger = Logger.getLogger(WorkoutController.class);

    @Autowired
    private WorkoutService workoutService;

    @CrossOrigin(origins = "*")
    @PostMapping("workout/save")
    public ResponseEntity<Workout> save(@RequestBody Workout workout) {
        final long start = System.currentTimeMillis();
        final String _methodName = "save(workout)";

        LoggingUtility.logEntering(logger, _methodName, workout.toString());

        Workout result = workoutService.save(workout);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @CrossOrigin(origins = "*")
    @PostMapping("workout/{idWorkout}/addStep")
    public ResponseEntity<Workout> addStep(@PathVariable Long idWorkout, @RequestBody Step step) {
        final long start = System.currentTimeMillis();
        final String _methodName = "addStep(idWorkout, step)";

        LoggingUtility.logEntering(logger, _methodName, idWorkout.toString());

        Workout result = workoutService.addStep(idWorkout, step);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @CrossOrigin(origins = "*")
    @PostMapping("step/{idStep}/del")
    public ResponseEntity deleteStep(@PathVariable Long idStep) {
        final long start = System.currentTimeMillis();
        final String _methodName = "deleteStep(idStep)";

        LoggingUtility.logEntering(logger, _methodName, idStep.toString());

        workoutService.deleteStep(idStep);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok().build();
    }

    @CrossOrigin(origins = "*")
    @PostMapping("workout/{idWorkout}/del")
    public ResponseEntity deleteWorkout(@PathVariable Long idWorkout) {
        final long start = System.currentTimeMillis();
        final String _methodName = "deleteWorkout(idWorkout)";

        LoggingUtility.logEntering(logger, _methodName, idWorkout.toString());

        workoutService.deleteWorkout(idWorkout);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok().build();
    }

    @CrossOrigin(origins = "*")
    @GetMapping("workout/{idWorkout}")
    public ResponseEntity<Workout> get(@PathVariable Long idWorkout) {
        final long start = System.currentTimeMillis();
        final String _methodName = "get(idWorkout)";

        LoggingUtility.logEntering(logger, _methodName, idWorkout.toString());

        Workout result = workoutService.get(idWorkout);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("workout/athlete/{idAthlete}")
    public ResponseEntity<List<Workout>> listByAthlete(@PathVariable Long idAthlete) {
        final long start = System.currentTimeMillis();
        final String _methodName = "listByAthlete(idAthlete)";

        LoggingUtility.logEntering(logger, _methodName, idAthlete.toString());

        List<Workout> result = workoutService.listByAthlete(idAthlete);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @CrossOrigin(origins = "*")
    @PostMapping("workout/{idWorkout}/flag")
    public ResponseEntity<Workout> flagWorkout(@PathVariable Long idWorkout) {
        final long start = System.currentTimeMillis();
        final String _methodName = "flagWorkout(idWorkout)";
        LoggingUtility.logEntering(logger, _methodName, idWorkout.toString());

        Workout result = workoutService.flagWorkout(idWorkout);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }
}
