package com.synit.worbiservice.services.workout;

import com.synit.worbiservice.dao.*;
import com.synit.worbiservice.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class WorkoutService {

    @Autowired
    private WorkoutDAO workoutDAO;
    @Autowired
    private AthleteDAO athleteDAO;
    @Autowired
    private CoachDAO coachDAO;
    @Autowired
    private SportDAO sportDAO;
    @Autowired
    private StepDAO stepDAO;

    public Workout save(Workout workout) {

        Workout result = new Workout();
        if (workout.getAthlete() != null && workout.getAthlete().getIdAthlete() != null) {
            Athlete athlete = athleteDAO.findOne(workout.getAthlete().getIdAthlete());
            workout.setAthlete(athlete);

            if (workout.getCoach() != null && workout.getCoach().getIdCoach() != null) {
                Coach coach = coachDAO.findOne(workout.getCoach().getIdCoach());
                workout.setCoach(coach);

                if (workout.getSport() != null && workout.getSport().getIdSport() != null) {
                    Sport sport = sportDAO.findOne(workout.getSport().getIdSport());
                    workout.setSport(sport);

                    result = workoutDAO.save(workout);
                }
            }
        }

        return result;
    }

    public List<Workout> listByAthlete(Long idAthlete) {
        return workoutDAO.findAllByAthleteIdAthleteOrderByScheduleDateDesc(idAthlete);
    }

    public void deleteStep(Long idStep) {
        stepDAO.delete(idStep);
    }

    public void deleteWorkout(Long idWorkout) {
        workoutDAO.delete(idWorkout);
    }

    public Workout flagWorkout(Long idWorkout) {

        Workout workout = workoutDAO.findOne(idWorkout);

        if (workout.getDone() == null ) {
            workout.setDone(Boolean.FALSE);
        }
        workout.setDone(!workout.getDone());

        return workoutDAO.save(workout);
    }

    public Workout addStep(Long idWorkout, Step step) {
        Workout workout = workoutDAO.findOne(idWorkout);

        if (workout.getSteps().isEmpty()) {
            step.setStepOrder(0L);
        } else {
            int last = workout.getSteps().size() -1;
            step.setStepOrder(workout.getSteps().get(last).getStepOrder() + 1);
        }
        step.setWorkout(workout);
        stepDAO.save(step);

        return workoutDAO.findOne(idWorkout);
    }

    public Workout get(Long idWorkout) {
        return workoutDAO.findOne(idWorkout);
    }

}
