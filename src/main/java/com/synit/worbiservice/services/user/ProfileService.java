package com.synit.worbiservice.services.user;

import com.synit.worbiservice.entity.Athlete;
import com.synit.worbiservice.entity.Coach;
import com.synit.worbiservice.entity.User;
import com.synit.worbiservice.enums.ProfileType;
import com.synit.worbiservice.exception.UserNotFoundException;
import com.synit.worbiservice.services.user.vo.Profile;
import com.synit.worbiservice.services.user.vo.UserFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProfileService {

    @Autowired
    private UserService userService;

    @Autowired
    private AthleteService athleteService;

    @Autowired
    private CoachService coachService;


    public List<Profile> listUserProfiles(UserFilter filter) {

        List<Profile> profiles = new ArrayList<Profile>();

        List<User> users = userService.searchUserByNameOrEmail(filter.getQuery());
        buildProfile(profiles, users, filter.getProfileType());

        return profiles;
    }

    public List<Profile> listCoachProfiles(UserFilter filter) {

        List<Profile> profiles = new ArrayList<Profile>();

        List<Coach> coaches = coachService.searchByNameOrEmail(filter.getQuery());
        for (Coach coach: coaches) {
            Profile profile = new Profile();
            profile.setUser(coach.getCredential());
            profile.setCoach(coach);
            profile.setIsCoach(Boolean.TRUE);

            profiles.add(profile);
        }
        return profiles;
    }

    public List<Profile> listAthleteProfiles(UserFilter filter) {

        List<Profile> profiles = new ArrayList<Profile>();

        List<Athlete> athletes = athleteService.searchByNameOrEmail(filter.getQuery());
        for (Athlete athlete: athletes) {
            Profile profile = new Profile();
            profile.setUser(athlete.getCredential());
            profile.setAthlete(athlete);
            profile.setIsAthlete(Boolean.TRUE);

            profiles.add(profile);
        }
        return profiles;
    }

    public Profile getUserProfile(UserFilter filter) throws UserNotFoundException {

        Profile result = null;

        List<Profile> profiles = new ArrayList<Profile>();
        List<User> users = userService.list(filter.getId());
        buildProfile(profiles, users, filter.getProfileType());

        if (profiles.size() > 0) {
            result = profiles.get(0);
        } else {
            throw new UserNotFoundException();
        }

        return result;
    }

    private void buildProfile(List<Profile> profiles, List<User> users, ProfileType profileType) {
        for (User user : users) {
            Profile profile = new Profile();

            if(user != null) {
                profile.setUser(user);

                List<Athlete> athletes = athleteService.searchByIdUser(user.getIdUser());
                List<Coach> coaches = coachService.searchByIdUser(user.getIdUser());

                if (athletes.size() > 0 && (profileType == null || ProfileType.ATHLETE.equals(profileType))) {
                    profile.setAthlete(athletes.get(0));
                }
                if (coaches.size() > 0 && (profileType == null || ProfileType.COACH.equals(profileType))) {
                    profile.setCoach(coaches.get(0));
                }

                profiles.add(profile);
            }
        }
    }

}
