package com.synit.worbiservice.services.user;

import com.synit.worbiservice.controller.BaseController;
import com.synit.worbiservice.exception.DuplicatedUserException;
import com.synit.worbiservice.exception.UserNotFoundException;
import com.synit.worbiservice.services.user.vo.Profile;
import com.synit.worbiservice.services.user.vo.UserFilter;
import com.synit.worbiservice.util.log.LoggingUtility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@RestController
public class UserProfileController extends BaseController {

    private static Logger logger = Logger.getLogger(UserProfileController.class);

    @Autowired
    private ProfileService profileService;
    @Autowired
    private UserService userService;
    @Autowired
    private AthleteService athleteService;
    @Autowired
    private CoachService coachService;

    @CrossOrigin(origins = "*")
    @GetMapping("profiles/{query}")
    public ResponseEntity<List<Profile>> listByQuery(@PathVariable String query) {
        final long start = System.currentTimeMillis();
        final String _methodName = "listByQuery(query)";

        LoggingUtility.logEntering(logger, _methodName, query);

        UserFilter filter = new UserFilter();
        filter.setQuery(query);
        List<Profile> result = profileService.listUserProfiles(filter);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("coaches/{query}")
    public ResponseEntity<List<Profile>> listCoachByQuery(@PathVariable String query) {
        final long start = System.currentTimeMillis();
        final String _methodName = "listByCoachQuery(query)";

        LoggingUtility.logEntering(logger, _methodName, query);

        UserFilter filter = new UserFilter();
        filter.setQuery(query);
        List<Profile> result = profileService.listCoachProfiles(filter);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("athletes/{query}")
    public ResponseEntity<List<Profile>> listAthleteByQuery(@PathVariable String query) {
        final long start = System.currentTimeMillis();
        final String _methodName = "listByCoachQuery(query)";

        LoggingUtility.logEntering(logger, _methodName, query);

        UserFilter filter = new UserFilter();
        filter.setQuery(query);
        List<Profile> result = profileService.listAthleteProfiles(filter);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("profile/{idUser}")
    public ResponseEntity<Profile> listById(@PathVariable Long idUser) throws UserNotFoundException {
        final long start = System.currentTimeMillis();
        final String _methodName = "listById(idUser)";

        LoggingUtility.logEntering(logger, _methodName, String.valueOf(idUser));

        UserFilter filter = new UserFilter();
        filter.setId(idUser);
        Profile result = profileService.getUserProfile(filter);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

}
