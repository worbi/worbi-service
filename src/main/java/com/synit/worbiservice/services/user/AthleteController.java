package com.synit.worbiservice.services.user;

import com.synit.worbiservice.controller.BaseController;
import com.synit.worbiservice.entity.Athlete;
import com.synit.worbiservice.entity.Coach;
import com.synit.worbiservice.util.log.LoggingUtility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@RestController
public class AthleteController extends BaseController {

    private static Logger logger = Logger.getLogger(AthleteController.class);

    @Autowired
    private AthleteService athleteService;

    @CrossOrigin(origins = "*")
    @PostMapping("athlete/save")
    public ResponseEntity<Athlete> save(@RequestBody Athlete athlete) {

        final long start = System.currentTimeMillis();
        final String _methodName = "save(athlete)";

        LoggingUtility.logEntering(logger, _methodName, athlete.toString());

        Athlete result = athleteService.save(athlete);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @CrossOrigin(origins = "*")
    @PostMapping("athlete/{idAthlete}/coach/add")
    public ResponseEntity addCoach(@PathVariable Long idAthlete, @RequestBody Coach coach) {

        final long start = System.currentTimeMillis();
        final String _methodName = "addCoach(idAthlete, coach)";
        LoggingUtility.logEntering(logger, _methodName, idAthlete.toString(), coach.toString());

        Boolean success = athleteService.addCoach(idAthlete, coach);

        LoggingUtility.logTimedMessage(logger, _methodName, start);

        ResponseEntity response;
        if (success) {
            response = ResponseEntity.ok().build();
        } else {
            response = ResponseEntity.badRequest().build();
        }

        return response;
    }

    @CrossOrigin(origins = "*")
    @PostMapping("athlete/{idAthlete}/coach/del")
    public ResponseEntity deleteCoach(@PathVariable Long idAthlete, @RequestBody Coach coach) {

        final long start = System.currentTimeMillis();
        final String _methodName = "deleteCoach(idAthlete, coach)";
        LoggingUtility.logEntering(logger, _methodName, idAthlete.toString(), coach.toString());

        Boolean success = athleteService.deleteCoach(idAthlete, coach);

        LoggingUtility.logTimedMessage(logger, _methodName, start);

        ResponseEntity response;
        if (success) {
            response = ResponseEntity.ok().build();
        } else {
            response = ResponseEntity.badRequest().build();
        }

        return response;
    }

    @CrossOrigin(origins = "*")
    @GetMapping("athlete")
    public ResponseEntity<List<Athlete>> list() {
        final long start = System.currentTimeMillis();
        final String _methodName = "list()";

        LoggingUtility.logEntering(logger, _methodName);

        List<Athlete> result = athleteService.list();

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("athlete/{idAthlete}")
    public ResponseEntity<Athlete> get(@PathVariable Long idAthlete) {
        final long start = System.currentTimeMillis();
        final String _methodName = "get(idAthlete)";
        LoggingUtility.logEntering(logger, _methodName, idAthlete.toString());

        Athlete result = athleteService.get(idAthlete);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("athlete.user/{idUser}")
    public ResponseEntity<List<Athlete>> searchByIdUser(@PathVariable Long idUser) {
        final long start = System.currentTimeMillis();
        final String _methodName = "searchByIdUser(query)";
        LoggingUtility.logEntering(logger, _methodName, String.valueOf(idUser));

        List<Athlete> result = athleteService.searchByIdUser(idUser);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

}
