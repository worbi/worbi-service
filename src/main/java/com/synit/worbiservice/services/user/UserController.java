package com.synit.worbiservice.services.user;

import com.synit.worbiservice.DatabaseConfig;
import com.synit.worbiservice.controller.BaseController;
import com.synit.worbiservice.entity.Athlete;
import com.synit.worbiservice.entity.User;
import com.synit.worbiservice.exception.DuplicatedUserException;
import com.synit.worbiservice.exception.UserNotAllowedException;
import com.synit.worbiservice.exception.UserNotFoundException;
import com.synit.worbiservice.services.user.vo.ChangePassword;
import com.synit.worbiservice.util.log.LoggingUtility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@RestController
public class UserController extends BaseController {

    private static Logger logger = Logger.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private AthleteService athleteService;

    @Autowired
    private DatabaseConfig databaseConfig;

    @CrossOrigin(origins = "*")
    @PostMapping("user/save")
    public ResponseEntity<User> save(@RequestBody User user) throws DuplicatedUserException {
        final long start = System.currentTimeMillis();
        final String _methodName = "save(user)";
        LoggingUtility.logEntering(logger, _methodName, user.toString());

        User result = userService.save(user);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @CrossOrigin(origins = "*")
    @PostMapping("user/resetpassword")
    public ResponseEntity<User> resetPassword(@RequestBody String email) throws UserNotFoundException, DuplicatedUserException {
        final long start = System.currentTimeMillis();
        final String _methodName = "resetPassword(email)";
        LoggingUtility.logEntering(logger, _methodName, email);

        User user = userService.findByEmail(email);
        user.setPassword("1234");
        User result = userService.save(user);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("user")
    public ResponseEntity<List<User>> list() {
        return getById(null);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("user/{id}")
    public ResponseEntity<List<User>> getById(@PathVariable Long id) {
        final long start = System.currentTimeMillis();
        final String _methodName = "getById(id)";
        LoggingUtility.logEntering(logger, _methodName, String.valueOf(id));

        List<User> result = userService.list(id);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("user-query/{query}")
    public ResponseEntity<List<User>> searchByNameOrEmail(@PathVariable String query) {
        final long start = System.currentTimeMillis();
        final String _methodName = "searchByNameOrEmail(query)";
        LoggingUtility.logEntering(logger, _methodName, query);

        List<User> result = userService.searchUserByNameOrEmail(query);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @CrossOrigin(origins = "*")
    @DeleteMapping("user/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        final long start = System.currentTimeMillis();
        final String _methodName = "delete(id)";
        LoggingUtility.logEntering(logger, _methodName);

        userService.delete(id);

        LoggingUtility.logTimedMessage(logger, _methodName, start);

        return ResponseEntity.ok().build();
    }

    @CrossOrigin(origins = "*")
    @PostMapping("user/changepassword")
    public ResponseEntity<User> changePassword(@RequestBody ChangePassword changePassword) throws UserNotAllowedException, DuplicatedUserException {
        final long start = System.currentTimeMillis();
        final String _methodName = "changePassword(user)";
        LoggingUtility.logEntering(logger, _methodName, changePassword.toString());

        User user = new User();
        User result = new User();

        if (changePassword.getIdUser() != null){

            user.setIdUser(changePassword.getIdUser());


            if (changePassword.getCurrentPassword() != null && changePassword.getNewPassword() != null) {
                user = userService.findById(user.getIdUser());
                Boolean userAllowed = databaseConfig.passwordEncoder().matches(changePassword.getCurrentPassword().getPassword(), user.getPassword());

                if (userAllowed) {
                    if (changePassword.getNewPassword().compare()) {
                        user.setPassword(changePassword.getNewPassword().getPassword());
                        result = userService.save(user);
                    } else {
                        //passwords don't match
                        throwUserNotAllowed(_methodName);
                    }
                } else {
                    //current password not match
                    throwUserNotAllowed(_methodName);
                }
            }
         }

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    private void throwUserNotAllowed(String methodName) throws UserNotAllowedException {
        UserNotAllowedException ex = new UserNotAllowedException();
        LoggingUtility.logError(logger, methodName, ex.getMessage());
        throw ex;
    }
}
