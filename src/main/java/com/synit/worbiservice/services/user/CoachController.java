package com.synit.worbiservice.services.user;

import com.synit.worbiservice.controller.BaseController;
import com.synit.worbiservice.entity.Athlete;
import com.synit.worbiservice.entity.Coach;
import com.synit.worbiservice.util.log.LoggingUtility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@RestController
public class CoachController  extends BaseController {

    private static Logger logger = Logger.getLogger(CoachController.class);

    @Autowired
    private CoachService coachService;

    @CrossOrigin(origins = "*")
    @PostMapping("coach/save")
    public ResponseEntity<Coach> save(@RequestBody Coach coach) {
        final long start = System.currentTimeMillis();
        final String _methodName = "save(coach)";

        LoggingUtility.logEntering(logger, _methodName, coach.toString());

        Coach result = coachService.save(coach);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("coach")
    public ResponseEntity<List<Coach>> list() {
        final long start = System.currentTimeMillis();
        final String _methodName = "list()";

        LoggingUtility.logEntering(logger, _methodName);

        List<Coach> result = coachService.list();

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("coach/{idCoach}")
    public ResponseEntity<Coach> get(@PathVariable Long idCoach) {
        final long start = System.currentTimeMillis();
        final String _methodName = "get(idCoach)";
        LoggingUtility.logEntering(logger, _methodName, idCoach.toString());

        Coach result = coachService.get(idCoach);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("coach.user/{idUser}")
    public ResponseEntity<List<Coach>> searchByIdUser(@PathVariable Long idUser) {
        final long start = System.currentTimeMillis();
        final String _methodName = "searchByIdUser(query)";
        LoggingUtility.logEntering(logger, _methodName, String.valueOf(idUser));

        List<Coach> result = coachService.searchByIdUser(idUser);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @CrossOrigin(origins = "*")
    @PostMapping("coach/{idCoach}/athlete/add")
    public ResponseEntity addAthlete(@PathVariable Long idCoach, @RequestBody Athlete athlete) {

        final long start = System.currentTimeMillis();
        final String _methodName = "addAthlete(idCoach, athlete)";
        LoggingUtility.logEntering(logger, _methodName, idCoach.toString(), athlete.toString());

        Boolean success = coachService.addAthlete(idCoach, athlete);

        LoggingUtility.logTimedMessage(logger, _methodName, start);

        ResponseEntity response;
        if (success) {
            response = ResponseEntity.ok().build();
        } else {
            response = ResponseEntity.badRequest().build();
        }

        return response;
    }

    @CrossOrigin(origins = "*")
    @PostMapping("coach/{idCoach}/athlete/del")
    public ResponseEntity deleteAthlete(@PathVariable Long idCoach, @RequestBody Athlete athlete) {

        final long start = System.currentTimeMillis();
        final String _methodName = "deleteAthlete(idAthlete, coach)";
        LoggingUtility.logEntering(logger, _methodName, idCoach.toString(), athlete.toString());

        Boolean success = coachService.deleteAthlete(idCoach, athlete);

        LoggingUtility.logTimedMessage(logger, _methodName, start);

        ResponseEntity response;
        if (success) {
            response = ResponseEntity.ok().build();
        } else {
            response = ResponseEntity.badRequest().build();
        }

        return response;
    }

}
