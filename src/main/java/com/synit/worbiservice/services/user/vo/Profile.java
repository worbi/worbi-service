package com.synit.worbiservice.services.user.vo;

import com.synit.worbiservice.entity.Athlete;
import com.synit.worbiservice.entity.Coach;
import com.synit.worbiservice.entity.User;
import org.springframework.beans.factory.annotation.Value;

import java.io.Serializable;

public class Profile {



    private User user;
    private Athlete athlete;
    private Coach coach;

    private Boolean isAthlete = false;
    private Boolean isCoach = false;

    public Profile() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Athlete getAthlete() {
        return athlete;
    }

    public void setAthlete(Athlete athlete) {
        this.athlete = athlete;
        if (this.athlete != null) {
            this.setIsAthlete(true);
        }
    }

    public Coach getCoach() {
        return coach;
    }

    public void setCoach(Coach coach) {
        this.coach = coach;
        if (this.coach != null) {
            this.setIsCoach(true);
        }
    }

    public Boolean getIsAthlete() {
        return this.isAthlete;
    }

    public Boolean getIsCoach() {
        return this.isCoach;
    }

    public Boolean setIsAthlete(Boolean isAthlete) {
        this.isAthlete = isAthlete;
        return this.isAthlete;
    }

    public Boolean setIsCoach(Boolean isCoach) {
        this.isCoach = isCoach;
        return this.isCoach;
    }
}
