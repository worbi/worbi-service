package com.synit.worbiservice.services.user;

import com.synit.worbiservice.dao.UserDAO;
import com.synit.worbiservice.entity.User;
import com.synit.worbiservice.exception.DuplicatedUserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserService {

    @Autowired
    private UserDAO userDao;

    public User save(User user) throws DuplicatedUserException {

        User result;
        if (user.getIdUser() != null) {

            User fromDB = userDao.findOne(user.getIdUser());
            user.setPasswordDecoded(fromDB.getPassword());

            result = userDao.save(user);
        } else {

            User find = userDao.findByEmail(user.getEmail());
            if (find == null) {

                if (user.getPassword() == null || "".equals(user.getPassword())) {
                    user.setPassword("1234");
                }

                result = userDao.save(user);
            } else {
                result = find;
                throw new DuplicatedUserException();
            }
        }

        return result;
    }

    public List<User> searchUserByNameOrEmail(String name) {

        List<User> results = new ArrayList<>();
        if (name != null) {
            results = userDao.findByNameContainingIgnoreCaseOrEmailContainingIgnoreCaseOrderByName(name,name);
        }

        return results;
    }

    public List<User> list(Long id) {

        if (id != null) {

            List<User> result = new ArrayList<>();
            result.add(userDao.findOne(id));

            return result;
        } else {
            return list();
        }
    }


    public User findById(Long idUser) {
        return userDao.findUserByIdUser(idUser);
    }

    public User findByEmail(String email) {
        return userDao.findByEmail(email);
    }


    public List<User> list() {
        return userDao.findAll();
    }

    public void delete(Long id) {
        userDao.delete(id);
    }

}
