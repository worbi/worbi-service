package com.synit.worbiservice.services.user;

import com.synit.worbiservice.dao.AthleteDAO;
import com.synit.worbiservice.dao.CoachDAO;
import com.synit.worbiservice.entity.Athlete;
import com.synit.worbiservice.entity.Coach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component
public class CoachService {

    @Autowired
    private CoachDAO coachDao;

    @Autowired
    private AthleteDAO athleteDAO;

    public Coach save(Coach coach) {

        if (coach.getIdCoach() != null) {
            List<Athlete> athletes = athleteDAO.findAllByCoachIdCoach(coach.getIdCoach());
            coach.setAthletes(athletes);
        }

        Coach result = coachDao.save(coach);
        return result;
    }

    public List<Coach> list() {
        List<Coach> fromDb = coachDao.findAll();

        List<Coach> result = new ArrayList<Coach>();
        for (Coach coach: fromDb) {
            List<Athlete> athletes = athleteDAO.findAllByCoachIdCoach(coach.getIdCoach());
            coach.setAthletes(athletes);

            result.add(coach);
        }

        return result;
    }

    public Coach get(Long idCoach) {
        return coachDao.findOne(idCoach);
    }

    public List<Coach> searchByIdUser(Long idUser) {
        return coachDao.findByCredentialIdUser(idUser);
    }

    public List<Coach> searchByNameOrEmail(String name) {
        return coachDao.findAllByCredentialNameContainingIgnoreCaseOrCredentialEmailContainingIgnoreCase(name, name);
    }

    public boolean addAthlete(Long idCoach, Athlete athlete) {

        if ((idCoach != null) && (athlete != null) && (athlete.getIdAthlete() != null)) {

            Athlete getAthlete = athleteDAO.findOne(athlete.getIdAthlete());
            Coach coach = coachDao.findOne(idCoach);

            if (coach.getAthletes().size() > 0) {
                for (Athlete a: coach.getAthletes()) {
                    if (a.getIdAthlete().equals(getAthlete.getIdAthlete())) {
                        return false;
                    }
                }
            }

            coach.getAthletes().add(getAthlete);
            coachDao.save(coach);

            return true;
        }

        return false;
    }

    public boolean deleteAthlete(Long idCoach, Athlete athlete) {

        if ((idCoach != null) && (athlete != null) && (athlete.getIdAthlete() != null)) {

            Athlete getAthlete = athleteDAO.findOne(athlete.getIdAthlete());
            Coach coach = coachDao.findOne(idCoach);

            if (coach.getAthletes().size() > 0) {
                List<Athlete> coaches = coach.getAthletes();
                coach.getAthletes().remove(getAthlete);
            }

            coachDao.save(coach);
            return true;
        }

        return false;
    }

}
