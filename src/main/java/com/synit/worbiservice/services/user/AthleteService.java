package com.synit.worbiservice.services.user;

import com.synit.worbiservice.dao.AthleteDAO;
import com.synit.worbiservice.dao.CoachDAO;
import com.synit.worbiservice.dao.TrainingWeekdayDAO;
import com.synit.worbiservice.entity.Athlete;
import com.synit.worbiservice.entity.Coach;
import com.synit.worbiservice.entity.TrainingWeekday;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AthleteService {

    @Autowired
    private AthleteDAO athleteDAO;

    @Autowired
    private CoachDAO coachDAO;

    @Autowired
    private TrainingWeekdayDAO trainingWeekdayDAO;

    public Athlete save(Athlete athlete) {

        if (athlete.getIdAthlete() != null) {

            Athlete fromDB = athleteDAO.findOne(athlete.getIdAthlete());
            athlete.setCredential(fromDB.getCredential());
            athlete.setCoach(fromDB.getCoach());

        }

        Athlete saved = athleteDAO.save(athlete);

        if (athlete.getTrainingWeekdays() !=  null) {
            List<TrainingWeekday> trainingWeekdays = athlete.getTrainingWeekdays();
            for (int i = 0; i < trainingWeekdays.size(); i++) {
                trainingWeekdays.get(i).setAthlete(saved);
            }
            trainingWeekdayDAO.deleteByAthleteIdAthlete(saved.getIdAthlete());
            List<TrainingWeekday> savedTranings = trainingWeekdayDAO.save(trainingWeekdays);
            saved.setTrainingWeekdays(savedTranings);
        }

        return saved;
    }

    public boolean addCoach(Long idAthlete, Coach coach) {

        if ((idAthlete != null) && (coach != null) && (coach.getIdCoach() != null)) {

            Coach getCoach = coachDAO.findOne(coach.getIdCoach());
            Athlete athlete = athleteDAO.findOne(idAthlete);

            if (athlete.getCoach().size() > 0) {
                for (Coach c: athlete.getCoach()) {
                    if (c.getIdCoach().equals(getCoach.getIdCoach())) {
                        return false;
                    }
                }
            }

            athlete.getCoach().add(getCoach);
            athleteDAO.save(athlete);

            return true;
        }

        return false;
    }

    public boolean deleteCoach(Long idAthlete, Coach coach) {

        if ((idAthlete != null) && (coach != null) && (coach.getIdCoach() != null)) {

            Coach getCoach = coachDAO.findOne(coach.getIdCoach());
            Athlete athlete = athleteDAO.findOne(idAthlete);

            if (athlete.getCoach().size() > 0) {
                List<Coach> coaches = athlete.getCoach();
                athlete.getCoach().remove(getCoach);
            }

            athleteDAO.save(athlete);

            return true;
        }

        return false;
    }

    public List<Athlete> searchByIdUser(Long idUser) {
        return athleteDAO.findByCredentialIdUser(idUser);
    }

    public List<Athlete> searchByNameOrEmail(String name) {
        return athleteDAO.findAllByCredentialNameContainingIgnoreCaseOrCredentialEmailContainingIgnoreCase(name, name);
    }

    public List<Athlete> list() {
        return athleteDAO.findAll();
    }

    public Athlete get(Long idAthlete) {
        return athleteDAO.findOne(idAthlete);
    }

}
