package com.synit.worbiservice.services.user.vo;

public class Password {

    private String confirm;
    private String password;


    public String getConfirm() {
        return confirm;
    }

    public void setConfirm(String confirm) {
        this.confirm = confirm;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean compare() {

        Boolean result = false;

        if (this.password != null && this.confirm != null) {
            if (this.password.equals(this.confirm)) {
                result = true;
            }
        }

        return result;
    }
}
