package com.synit.worbiservice.controller;

import com.synit.worbiservice.exception.DuplicatedUserException;
import com.synit.worbiservice.exception.UserNotAllowedException;
import com.synit.worbiservice.exception.UserNotFoundException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("services/v1")
public class BaseController {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({UserNotFoundException.class, EmptyResultDataAccessException.class})
    public void handleUserNotFound() {

    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(UserNotAllowedException.class)
    public void handleUserNotAllowed() {

    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(DuplicatedUserException.class)
    public void handleConflict() {

    }

}
