package com.synit.worbiservice.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.databind.ser.Serializers;
import com.synit.worbiservice.dao.*;
import com.synit.worbiservice.entity.Athlete;
import com.synit.worbiservice.entity.TrainingWeekday;
import com.synit.worbiservice.enums.WeekdayEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.synit.worbiservice.entity.User;

@RestController
public class TestController extends BaseController {

	@Autowired
	private CoachDAO coachDAO;
	@Autowired
	private UserDAO userDAO;
	@Autowired
	private AthleteDAO athleteDAO;

	@Autowired
	private TrainingWeekdayDAO trainingWeekdayDAO;
	@Autowired
	private SportDAO sportDao;

	@CrossOrigin(origins = "*")
    @RequestMapping(value="check",method=RequestMethod.GET)
    @Produces({ MediaType.APPLICATION_JSON })
    @ResponseStatus(HttpStatus.OK)
	public String check() {
		return "Hello World";
	}
	
	@CrossOrigin(origins = "*")
    @RequestMapping(value="load",method=RequestMethod.GET)
    @Produces({ MediaType.APPLICATION_JSON })
    @ResponseStatus(HttpStatus.OK)
	public String load() {

		User u = new User();
		Athlete a = new Athlete();

		u.setEmail("martins.rs@gmail.com");
		u.setPassword("lalala");
		u.setIsAdmin(Boolean.FALSE);
		u.setName("Eduardo Martins");

		u = userDAO.save(u);

		if (u != null) {
            a.setCredential(userDAO.findOne(u.getIdUser()));
            a.setSports(sportDao.findAll());
            a = athleteDAO.save(a);

            TrainingWeekday t1 = new TrainingWeekday();
            t1.setAvailableTime(45d);
            //t1.setWeekday(WeekdayEnum.SUNDAY);
            //t1.setAthlete(athleteDAO.findOne(1L));

            TrainingWeekday t2 = new TrainingWeekday();
            t2.setAvailableTime(60d);
            //t2.setWeekday(WeekdayEnum.MONDAY);
            //t2.setAthlete(athleteDAO.findOne(1L));

            a.setTrainingWeekdays(new ArrayList<>());
            a.getTrainingWeekdays().add(t1);
            a.getTrainingWeekdays().add(t2);


            trainingWeekdayDAO.save(a.getTrainingWeekdays());
        }

		return "loaded";
	}
	
	
	
}
